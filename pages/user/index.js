let App = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    userInfo: {},
    orderCount: {},
	isData: false,
    words: {},
    user: {},
    dealer: {},
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // 获取当前用户信息
    this.getUserDetail();
    // 获取分销商中心数据
	this.getDealerCenter();
  },
  /**
 * 点击登录
 */
  login: function (e) {
    App.authorlogin(e, this);
  },
  commonNav: function () {
    this.setData({
      nav_select: !this.data.nav_select
    });
    wx.hideTabBar({});
  },
  /**
   * 获取当前用户信息
   */
  getUserDetail: function () {
    let _this = this;
    if (wx.getStorageSync('user_id')){
    App._get('index/detail', {}, function (result) {
      _this.setData(result.data);
    });
    }
  },
  
   /**
   * 获取分销商中心数据
   */
  getDealerCenter: function() {
    let _this = this;
    if (wx.getStorageSync('user_id')) {
    App._get('dealer/center', {}, function(result) {
      let data = result.data;
      data.isData = true;
      // 设置当前页面标题
      _this.setData({dealer:data});
    });
    }
  },

  

})
Page({
    data: {
        list: [
            {
                id: 'wheel',
                sub: 'wheel',
                name: '大转盘',
				state:1
            },
            {
                id: 'fruitMachine',
                sub: 'fruitMachine',
                name: '水果机',
				state:0
            },
			{
                id: 'scratch',
                sub: 'scratch',
                name: '刮刮乐',
				state:0
            },
            {
                id: 'slotMachine',
                sub: 'slotMachine',
                name: '老虎机',
				state:0
            },
            {
                id: 'gridcard',
                sub: 'gridcard',
                name: '九宫格翻纸牌',
				state:0
            },
            {
                id: 'shake',
                sub: 'shake',
                name: '摇一摇',
				state:0
            },
            {
                id: 'gestureLock',
                sub: 'gestureLock',
                name: '手势解锁',
				state:0
            }
        ]
    }
})

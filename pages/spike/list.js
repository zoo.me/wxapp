const App = getApp();
var timestamp = Date.parse(new Date());  
var date = new Date(timestamp);
Page({
  data: {
    scrollHeight: null,
    showView: false,
    arrange: "",

    sortType: 'all', // 排序类型
    sortPrice: false, // 价格从低到高

    option: {},
    list: {},

    noList: true,
    no_more: false,

	time:'',
	times:{
		'08:00':8,
		'10:00':10,
		'12:00':12,
		'14:00':14,
		'16:00':16,
		'18:00':18,
		'20:00':20,
		'22:00':22,
	},
	
    page: 1,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(option) {
    let _this = this;
    // 设置商品列表高度
    _this.setListHeight();

    // 记录option
    _this.setData({
      option,
	  time:date.getHours()
    }, function() {
      // 获取商品列表
      _this.getGoodsList(true);
    });

  },

  /**
   * 获取商品列表
   */
  getGoodsList: function(is_super, page) {
    let _this = this;
    App._get('spike/spikeList', {
		time: _this.data.time,
    }, function(result) {
      let resultList = result.data.spikeList;
	  console.log(resultList)
	  if(resultList == false){
		_this.setData({
		  noList: false,
		});
	  }else{
		_this.setData({
		  list: resultList,
		});
	  }
    });
  },

  /**
   * 设置商品列表高度
   */
  setListHeight: function() {
    let _this = this;
    wx.getSystemInfo({
      success: function(res) {
        _this.setData({
          scrollHeight: res.windowHeight - 90,
        });
      }
    });
  },

  /**
   * 切换排序方式
   */
  switchSortType: function(e) {
    let _this = this,
      newSortType = e.currentTarget.dataset.type,
      newSortPrice = newSortType === 'price' ? !this.data.sortPrice : true;

    this.setData({
      list: {},
      page: 1,
      sortType: newSortType,
      sortPrice: newSortPrice
    }, function() {
      // 获取商品列表
      _this.getGoodsList(true);
    });
  },

  /**
   * 跳转筛选
   */
  // toSynthesize: function (t) {
  //   wx.navigateTo({
  //     url: "../category/screen?objectId="
  //   });
  // },

  /**
   * 切换列表显示方式
   */
  onChangeShowState: function() {
    let _this = this;
    _this.setData({
      showView: !_this.data.showView,
      arrange: _this.data.arrange ? "" : "arrange"
    });
  },

  /**
   * 下拉到底加载数据
   */
  bindDownLoad: function() {
    // 已经是最后一页
    if (this.data.page >= this.data.list.last_page) {
      this.setData({
        no_more: true
      });
      return false;
    }
    this.getGoodsList(false, ++this.data.page);
  },

  /**
   * 设置分享内容
   */
  onShareAppMessage: function() {
    return {
      title: "全部分类",
      desc: "",
      path: "/pages/category/index?" + params
    };
  },

  /**
   * 商品搜索
   */
  triggerSearch: function() {
    let pages = getCurrentPages();
    // 判断来源页面
    if (pages.length > 1 &&
      pages[pages.length - 2].route === 'pages/search/index') {
      wx.navigateBack();
      return;
    }
    // 跳转到商品搜索
    wx.navigateTo({
      url: '../search/index',
    })
  },

});
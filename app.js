/**
 * tabBar页面路径列表 (用于链接跳转时判断)
 * tabBarLinks为常量, 无需修改
 */
const tabBarLinks = [
  'pages/index/index',
  'pages/category/index',
  'pages/flow/index',
  'pages/user/index'
];

// 工具类
const util = require('./utils/util.js');

App({

  /**
   * 全局变量
   */
  globalData: {
    user_id: null,
    userInfo:{}
  },

  // api地址
  api_root: '',
  siteInfo: require('siteinfo.js'),

  /**
   * 生命周期函数--监听小程序初始化
   */
  onLaunch: function(e) {
	   
    // 设置api地址
    this.setApiRoot();
    // 小程序主动更新
    this.updateManager();
    // 小程序启动场景
    this.onStartupScene(e.query);
  },

  /**
   * 小程序启动场景
   */
  onStartupScene: function(query) {

    // 获取场景值
    let scene = this.getSceneData(query);
    // 记录推荐人id
    let refereeId = query.referee_id ? query.referee_id : scene.uid;
    refereeId > 0 && (this.saveRefereeId(refereeId));
  },

  /**
   * 记录推荐人id
   */
  saveRefereeId: function(refereeId) {
	
    if (!wx.getStorageSync('referee_id'))
      wx.setStorageSync('referee_id', refereeId);
  },

  /**
   * 获取场景值(scene)
   */
  getSceneData: function(query) {
    return query.scene ? util.scene_decode(query.scene) : {};
  },

  /**
   * 当小程序启动，或从后台进入前台显示，会触发 onShow
   */
  onShow: function(options) {
    // 获取小程序基础信息
    this.getAppBase();
  },

  /**
   * 设置api地址
   */
  setApiRoot: function() {
    this.api_root = this.siteInfo.siteroot + 'index.php?s=/api/';
  },

  /**
   * 获取小程序基础信息
   */
  getAppBase: function(callback) {
	
    let App = this;
    App._get('app/base', {}, function(result) {
      // 记录小程序基础信息   
      wx.setStorageSync('app', result.data.app);
      wx.setStorageSync('color1', result.data.app.color1);
	    wx.setStorageSync('color2', result.data.app.color2);
      callback && callback(result.data.app);
    });
  },

  /**
   * 执行用户登录
   */
  doLogin: function() {
	 
    // 保存当前页面
    let pages = getCurrentPages();
    if (pages.length) {
		let currentPage = pages[pages.length - 1];
		"pages/login/login" != currentPage.route &&
        wx.setStorageSync("currentPage", currentPage);
    }
    // 跳转授权页面
    //wx.navigateTo({
      //url: "/pages/login/login"
    //});
  },
  /**
   * 授权登录
   */
  authorlogin: function (e, _that) {

    let _this = this;
    if (e.detail.errMsg !== 'getUserInfo:ok') {
      return false;
    }
    wx.showLoading({
      title: "正在登录",
      mask: true
    });
    // 执行微信登录
    wx.login({
      success: function (res) {
        // 发送用户信息
        _this._post_form('user/login', {
          code: res.code,
          app_id: _this.siteInfo.appid,
          user_info: e.detail.rawData,
          encrypted_data: e.detail.encryptedData,
          iv: e.detail.iv,
          signature: e.detail.signature,
          referee_id: wx.getStorageSync('referee_id')
        }, function (result) {
          // 记录token user_id
          wx.setStorageSync('token', result.data.token);
          wx.setStorageSync('user_id', result.data.user_id);
          wx.setStorageSync('user_info', e.detail.rawData);
          wx.showTabBar({});

          _that.setData({
            nav_select: !_that.data.nav_select
          });
          _that.onShow();
          // 跳转回原页面
          //_this.navigateBack();
        }, false, function () {
          wx.hideLoading();
        });
      }
    });
  },

  /**
   * 授权成功 跳转回原页面
   */
  navigateBack: function () {

    // wx.navigateBack();
    //let currentPage = wx.getStorageSync('currentPage');

    /* 		wx.redirectTo({
          url: '/' + currentPage.route + '?' + App.urlEncode(currentPage.options)
        }); */
    /* wx.redirectTo({
      url: '/pages/index/index'
    }); */
  },
  /**
   * 当前用户id
   */
  getUserId: function() {
    return wx.getStorageSync('user_id');
  },

  /**
   * 显示成功提示框
   */
  showSuccess: function(msg, callback) {
    wx.showToast({
      title: msg,
      icon: 'success',
      duration: 1500,
      success: function() {
        callback && (setTimeout(function() {
          callback();
        }, 1500));
      }
    });
  },

  /**
   * 显示失败提示框
   */
  showError: function(msg, callback) {
    wx.showModal({
      title: '友情提示',
      content: msg,
      showCancel: false,
      success: function(res) {
        // callback && (setTimeout(function() {
        //   callback();
        // }, 1500));
        callback && callback();
      }
    });
  },

  /**
   * get请求
   */
  _get: function(url, data, success, fail, complete, check_login) {
    
    wx.showNavigationBarLoading();
    let App = this;
    // 构造请求参数
    data = data || {};
    data.app_id = App.siteInfo.appid;
		
		// 构造get请求
		let request = function() {
		  data.token = wx.getStorageSync('token');
		
		  wx.request({
			url: App.api_root + url,
			header: {
			  'content-type': 'application/json'
			},
			data: data,
			success: function(res) {
        let userinfo = App.validateUserInfo();
        if (userinfo === false)
          App.doLogin();
			  if (res.statusCode !== 200 || typeof res.data !== 'object') {
				App.showError('网络请求出错');
				return false;
			  }
			 
			  if (res.data.code === -1) {
				// 登录态失效, 重新登录
				wx.hideNavigationBarLoading();
				App.doLogin();
			  } else if (res.data.code === 0) {
				App.showError(res.data.msg, function() {
				  fail && fail(res);
				});
				return false;
			  } else {
				success && success(res.data);
			  }
			},
			fail: function(res) {
			  App.showError(res.errMsg, function() {
				fail && fail(res);
			  });
			},
			complete: function(res) {
			  wx.hideNavigationBarLoading();
			  complete && complete(res);
			},
		  });
		};


    // 判断是否需要验证登录
    check_login ? App.doLogin(request) : request();
  },

  /**
   * post提交
   */
  _post_form: function(url, data, success, fail, complete) {
	  
    let App = this;
    data.app_id = App.siteInfo.appid;
    data.token = wx.getStorageSync('token');
    wx.showNavigationBarLoading();
    wx.request({
      url: App.api_root + url,
      header: {
        'content-type': 'application/x-www-form-urlencoded',
      },
      method: 'POST',
      data: data,
      success: function(res) {
        if (res.statusCode !== 200 || typeof res.data !== 'object') {
          App.showError('网络请求出错');
          return false;
        }
        if (res.data.code === -1) {
          // 登录态失效, 重新登录
          wx.hideNavigationBarLoading();
          App.doLogin();
          return false;
        } else if (res.data.code === 0) {
          App.showError(res.data.msg, function() {
            fail && fail(res);
          });
          return false;
        }
        success && success(res.data);
      },
      fail: function(res) {
      
        App.showError(res.errMsg, function() {
          fail && fail(res);
        });
      },
      complete: function(res) {
        wx.hideNavigationBarLoading();
        // wx.hideLoading();
        complete && complete(res);
      }
    });
  },

  /**
   * 验证是否存在user_info
   */
  validateUserInfo: function() {
    let user_info = wx.getStorageSync('user_info');
    return !!wx.getStorageSync('user_info');
  },

  /**
   * 对象转URL
   */
  urlEncode: function(data) {
    var _result = [];
    for (var key in data) {
      var value = data[key];
      if (value.constructor == Array) {
        value.forEach(function(_value) {
          _result.push(key + "=" + _value);
        });
      } else {
        _result.push(key + '=' + value);
      }
    }
    return _result.join('&');
  },

  /**
   * 小程序主动更新
   */
  updateManager: function() {
    if (!wx.canIUse('getUpdateManager')) {
      return false;
    }
    const updateManager = wx.getUpdateManager();
    updateManager.onCheckForUpdate(function(res) {
      // 请求完新版本信息的回调
    });
    updateManager.onUpdateReady(function() {
      wx.showModal({
        title: '更新提示',
        content: '新版本已经准备好，即将重启应用',
        showCancel: false,
        success: function(res) {
          if (res.confirm) {
            // 新的版本已经下载好，调用 applyUpdate 应用新版本并重启
            updateManager.applyUpdate()
          }
        }
      });
    });
    updateManager.onUpdateFailed(function() {
      // 新的版本下载失败
      wx.showModal({
        title: '更新提示',
        content: '新版本下载失败',
        showCancel: false
      })
    });
  },

  /**
   * 获取tabBar页面路径列表
   */
  getTabBarLinks: function() {
    return tabBarLinks;
  },

  /**
   * 跳转到指定页面
   * 支持tabBar页面
   */
  navigationTo: function(url) {
    if (!url || url.length == 0) {
      return false;
    }
    let tabBarLinks = this.getTabBarLinks();
    // tabBar页面
    if (tabBarLinks.indexOf(url) > -1) {
      wx.switchTab({
        url: '/' + url
      });
    } else {
      // 普通页面
      wx.navigateTo({
        url: '/' + url
      });
    }
  },

  /**
   * 记录formId
   */
  saveFormId: function(formId) {
    let App = this;
    if (formId === 'the formId is a mock one') {
      return false;
    }
    App._post_form('formId/save', {
      formId: formId
    });
  },

});